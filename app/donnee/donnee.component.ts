import { Component, OnInit } from '@angular/core';
import { donnee } from '../Model/donnee';

@Component({
  selector: 'app-donnee',
  templateUrl: './donnee.component.html',
  styleUrls: ['./donnee.component.css']
})
export class DonneeComponent implements OnInit {
  
  listDonnees:donnee[]=[{nom:'Hamzaoui',prenom:'Refka',age:'24',datenaissance:'13-03-1996'},
  {nom:'Hamzaoui',prenom:'Refka',age:'24',datenaissance:'13-03-1996'},
  {nom:'Hamzaoui',prenom:'Refka',age:'24',datenaissance:'13-03-1996'}];

  constructor() { }

  ngOnInit() {
  }

  deleteItem(i){
    this.listDonnees.splice(i,1);
  }

}
